 # Datepicker component using TS & pure CSS
 
 ## Демо
 
 [https://my-datepicker.now.sh](https://my-datepicker.now.sh)
 
 <img src="screens/date-selecting.png" width="250" />
 <img src="screens/month-selecting.png" width="250" />
 <img src="screens/year-selecting.png" width="250" />
 
 ## Фичи
 
 * Выбор даты
 * Изменение месяца
 * Изменение года
 * Возможноть управлять с клавиатуры
 
 ## Пример
  ```js
        import { Datepicker } from 'path-to/datepicker.js';
        const datepicker = new Datepicker('container');
        /***
        * Your code
        ***/
  ```
  Конструктор принимает два параметр. Первый `id` элемента в котором должен отображаться компонент. 
  Второе необязательный параметр `settings` - опции которого привидено ниже
  
  *Первый аргументом можно также передать уже готовый элемент которого вы сами взяли из DOM-а каким либо селектором*
  *Будьте внимательнее при передачи id элемента. В случае некорректной id выкидает ошибку*
 
 ## Опции
 
 * **month** *(default: "текуший месяц")* *[number]* - Месяц *от 0 до 11* 
 * **year** *(default: "текуший год")* *[number]* - Год
 * **theme** *(default: "light")* *[SupportedTheme]* - Тема UI *на данный момент есть только одна тема light*
 * **openOn** *(default: "click")* *[string]* - Открыть на событие *можно указать и mouseover, mousedown и что-то сами захотите*
 * **lang** *(default: "ru")* *[SupportedLanguage]* - Язык *на данный момент есть только поддержка русского языка*
 * **autoClose** *(default: true)* *[boolean]* - Закрыть после выбор даты 
 * **keyBoardControls** *(default: true)* *[boolean]* - Выключать и включать поддержки управления с клавиатуры 
 
 ## API
 
 `.open()`
 
 `.close()`
 
 `.value` возвращает выбранное значание даты в формате `dd/mm/yyyy`
 
 ## Хотите изменить допустим тему или язык под себя ?
 
 Отлично тогда:
 
 1. Клонируйте репозиторий 
 `git clone https://gitlab.com/bin-umar/simple-calendar.git` 
 
 2. Установите зависимости 
 `npm i`
 
 3. Если хотите добавить язык заходите на директорию
    `src/modules/months-days.ts`
    Там точно также как и `ru` добавьте еще один язык в объект `languageList` у которого должны быть два обязательных полей:
    `weekDays` - Дни недели и `months` - имена месяцов.
    После этого вам нужно добавить этот язык в список поддерживаемых языков в файле:
    `src/intefaces/months-days.ts => SupportedLanguage`  
 
 4. А если хотите добавить тему или кастомизировать под себя тогда на тот же самый 
    `src/intefaces/months-days.ts => SupportedTheme` добавьте имя новой темы а всё остольное смотрите файли `css` найдите там 
    селекторы которые зависять от темы, проще говоря начинаються с названием `.datepicker_light`, скопируйте и переделайте свою.
    
