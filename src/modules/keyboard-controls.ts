import { MainBlock } from '../components/main-block/mainBlock.js';

export class KeyboardControls {
    protected mainBlock: MainBlock;
    protected controls: string[];
    constructor(block: MainBlock) {
        this.mainBlock = block;
        this.controls = ['month', 'year', 'previous', 'next'];

        if (!this.mainBlock.picker) { return; }
        this.mainBlock.picker.addEventListener('keydown', this.onPickerKeyDown);
    }

    public focusFirstElement(e: Event | KeyboardEvent) {
        if (e instanceof KeyboardEvent && e.code === 'Enter' && e.target instanceof HTMLElement) {
            this.focusPickupElement(0, e.target);
        }
    }

    private onPickerKeyDown = (e: KeyboardEvent) => {
        const target = e.target;
        if (target instanceof HTMLElement && target.dataset.id) {
            const text = target.innerText;
            const id = target.dataset.id;

            if (text.length > 2 && !this.controls.includes(id)) {
                this.onFourColumnKeyDown(e.key, target, +id);
            } else if (text.length <= 2 && !this.controls.includes(id)) {
                this.onSevenColumnKeyDown(e.key, target, +id);
            } else if (this.controls.includes(id)) {
                this.onControlsKeyDown(e.key, target, id);
            }
        }
    }

    private onControlsKeyDown(key: string, target: HTMLElement, id: string) {
        const controlId = this.controls.indexOf(id);
        if (!~controlId) { return; }

        switch (key) {
            case 'ArrowUp': target.blur(); break;
            case 'ArrowDown': {
                this.mainBlock.currentElement === 'day' ? this.focusPickupElement(controlId * 2, target) :
                    this.focusPickupElement(controlId, target);
            }                 break;
            case 'ArrowRight': {
                const rightControl = this.controls[controlId + 1];
                rightControl ? this.focusControlElement(rightControl, target) :
                    this.focusPickupElement(0, target);
            }                  break;
            case 'ArrowLeft': {
                const leftControl = this.controls[controlId - 1];
                this.focusControlElement(leftControl, target);
            }                 break;
        }
    }

    private onSevenColumnKeyDown(key: string, target: HTMLElement, id: number) {
        switch (key) {
            case 'ArrowUp': {
                switch (id) {
                    case 0: case 1: this.focusControlElement('month', target); break;
                    case 2:         this.focusControlElement('year', target); break;
                    case 3: case 4: this.focusControlElement('previous', target); break;
                    case 5: case 6: this.focusControlElement('next', target); break;
                    default: this.focusUpElement(id, 7);
                }
            }               break;
            case 'ArrowDown': this.focusPickupElement( id + 7, target); break;
            case 'ArrowRight': this.focusPickupElement(id + 1, target); break;
            case 'ArrowLeft': this.focusLeftElement(id, target); break;
        }
    }

    private onFourColumnKeyDown(key: string, target: HTMLElement, id: number) {
        switch (key) {
            case 'ArrowUp': {
                switch (id) {
                    case 0: this.focusControlElement('month', target); break;
                    case 1: this.focusControlElement('year', target); break;
                    case 2: this.focusControlElement('previous', target); break;
                    case 3: this.focusControlElement('next', target); break;
                    default: this.focusUpElement(id, 4);
                }
            }               break;
            case 'ArrowDown': this.focusPickupElement(id + 4, target); break;
            case 'ArrowRight': this.focusPickupElement(id + 1, target); break;
            case 'ArrowLeft': this.focusLeftElement(id, target); break;
        }
    }

    private focusControlElement(id: string, target: HTMLElement) {
        const element = document.querySelector(`#${this.mainBlock.id} div[data-id=${id}]`);
        // @ts-ignore
        if (element) { element.focus(); } else { target.blur(); }
    }

    private focusPickupElement(id: number, target: HTMLElement) {
        const element = document.querySelector(`#${this.mainBlock.id} td[data-id="${id}"]`);
        // @ts-ignore
        if (element) { element.focus(); } else { target.blur(); }
    }

    private focusUpElement(id: number, columnCount: number) {
        const upElement = document.querySelector(`#${this.mainBlock.id} td[data-id="${id - columnCount}"]`);
        // @ts-ignore
        if (upElement) { upElement.focus(); }
    }

    private focusLeftElement(id: number, target: HTMLElement) {
        const leftElement = document.querySelector(`#${this.mainBlock.id} td[data-id="${id - 1}"]`);
        // @ts-ignore
        if (leftElement) { leftElement.focus(); } else { this.focusControlElement('next', target); }
    }
}
