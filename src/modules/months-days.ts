export class MonthsDays {
    protected currentLanguage: ILanguage;
    constructor(language: SupportedLanguage) {
        this.currentLanguage = languageList[language];
    }

    get months(): IMonthName[] {
        return this.currentLanguage.months;
    }

    get weekDays(): IMonthName[] {
        return this.currentLanguage.weekDays;
    }
}

const languageList: { [language: string]: ILanguage } = {
    ru: {
        weekDays: [
            {
                longName: 'Понедельник',
                shortName: 'Пн',
            },
            {
                longName: 'Вторник',
                shortName: 'Вт',
            },
            {
                longName: 'Среда',
                shortName: 'Ср',
            },
            {
                longName: 'Четверг',
                shortName: 'Чт',
            },
            {
                longName: 'Пятница',
                shortName: 'Пт',
            },
            {
                longName: 'Суббота',
                shortName: 'Сб',
            },
            {
                longName: 'Воскресенье',
                shortName: 'Вс',
            },
        ] as IMonthName[],
        months: [
            {
                longName: 'Январь',
                shortName: 'Янв',
            },
            {
                longName: 'Февраль',
                shortName: 'Фев',
            },
            {
                longName: 'Март',
                shortName: 'Мар',
            },
            {
                longName: 'Апрель',
                shortName: 'Апр',
            },
            {
                longName: 'Май',
                shortName: 'Май',
            },
            {
                longName: 'Июнь',
                shortName: 'Июн',
            },
            {
                longName: 'Июль',
                shortName: 'Июл',
            },
            {
                longName: 'Август',
                shortName: 'Авг',
            },
            {
                longName: 'Сентябрь',
                shortName: 'Сен',
            },
            {
                longName: 'Октябрь',
                shortName: 'Окт',
            },
            {
                longName: 'Ноябрь',
                shortName: 'Ноя',
            },
            {
                longName: 'Декабрь',
                shortName: 'Дек',
            },
        ] as IMonthName[],
    } as ILanguage,
};
