import { DatepickerDate } from './date';

test('Quantity of days in month', () => {
    expect(DatepickerDate.daysInMonth(2019, 0)).toBe(31); // January
    expect(DatepickerDate.daysInMonth(2019, 1)).toBe(28); // February
    expect(DatepickerDate.daysInMonth(2019, 2)).toBe(31); // March
    expect(DatepickerDate.daysInMonth(2019, 3)).toBe(30); // April
    expect(DatepickerDate.daysInMonth(2019, 4)).toBe(31); // May
    expect(DatepickerDate.daysInMonth(2019, 5)).toBe(30); // June
    expect(DatepickerDate.daysInMonth(2019, 6)).toBe(31); // July
    expect(DatepickerDate.daysInMonth(2019, 7)).toBe(31); // August
    expect(DatepickerDate.daysInMonth(2019, 8)).toBe(30); // September
    expect(DatepickerDate.daysInMonth(2019, 9)).toBe(31); // October
    expect(DatepickerDate.daysInMonth(2019, 10)).toBe(30); // November
    expect(DatepickerDate.daysInMonth(2019, 11)).toBe(31); // December
});

test('Should return last days of previous mont in given number', () => {
    const previousMonthDays = DatepickerDate.previousMonthDays(2019, 4, 5);
    expect(previousMonthDays()).toBe(26);
    expect(previousMonthDays()).toBe(27);
    expect(previousMonthDays()).toBe(28);
    expect(previousMonthDays()).toBe(29);
    expect(previousMonthDays()).toBe(30);
    expect(previousMonthDays()).toBe(31);
});

test('Should return the index of firstDay of Month in week', () => {
    expect(DatepickerDate.firstDay(2019, 6)).toBe(1);
});

test('Should add 0 to number if it\'s less than 10', () => {
   expect(DatepickerDate.toTwoDigit(1)).toBe('01');
   expect(DatepickerDate.toTwoDigit(10)).toBe('10');
});
