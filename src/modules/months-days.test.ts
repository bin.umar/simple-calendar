import { MonthsDays } from './months-days';

test('Test whether all months and days have required fields', () => {
    const monthsDays = new MonthsDays('ru');

    monthsDays.months.forEach(m => {
       expect(!!m.shortName).toBe(true);
       expect(!!m.longName).toBe(true);
    });

    monthsDays.weekDays.forEach(m => {
        expect(!!m.shortName).toBe(true);
        expect(!!m.longName).toBe(true);
    });
});

test('Short return list of months of given language and are they correct', () => {
    const monthsDays = new MonthsDays('ru');

    expect(monthsDays.months[0].shortName).toBe('Янв');
    expect(monthsDays.months[1].shortName).toBe('Фев');
    expect(monthsDays.months[2].shortName).toBe('Мар');
    expect(monthsDays.months[3].shortName).toBe('Апр');
    expect(monthsDays.months[4].shortName).toBe('Май');
    expect(monthsDays.months[5].shortName).toBe('Июн');
    expect(monthsDays.months[6].shortName).toBe('Июл');
    expect(monthsDays.months[7].shortName).toBe('Авг');
    expect(monthsDays.months[8].shortName).toBe('Сен');
    expect(monthsDays.months[9].shortName).toBe('Окт');
    expect(monthsDays.months[10].shortName).toBe('Ноя');
    expect(monthsDays.months[11].shortName).toBe('Дек');
});

test('Short return list of days of week of given language and are they correct', () => {
    const monthsDays = new MonthsDays('ru');

    expect(monthsDays.weekDays[0].shortName).toBe('Пн');
    expect(monthsDays.weekDays[1].shortName).toBe('Вт');
    expect(monthsDays.weekDays[2].shortName).toBe('Ср');
    expect(monthsDays.weekDays[3].shortName).toBe('Чт');
    expect(monthsDays.weekDays[4].shortName).toBe('Пт');
    expect(monthsDays.weekDays[5].shortName).toBe('Сб');
    expect(monthsDays.weekDays[6].shortName).toBe('Вс');
});
