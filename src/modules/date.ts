export class DatepickerDate {
    public static daysInMonth(year: number, month: number): number {
        return 32 - new Date(year, month, 32).getDate();
    }

    public static previousMonthDays(year: number, month: number, index: number) {
        const days = DatepickerDate.daysInMonth(year, month);
        return function(): number {
            return days - index--;
        };
    }

    public static firstDay(year: number, month: number): number {
        return (new Date(year, month)).getDay();
    }

    public static toTwoDigit(num: number): string {
        if (num < 10) { return '0' + num; }
        return num.toString();
    }

    public static get todayDate(): number {
        return (new Date()).getDate();
    }

    public static get thisMonth(): number {
        return (new Date()).getMonth();
    }

    public static get thisYear(): number {
        return (new Date()).getFullYear();
    }
}
