import { Cell } from './components/cell/cell.js';
import { MainBlock } from './components/main-block/mainBlock.js';
import { DatepickerDate } from './modules/date.js';
import { KeyboardControls } from './modules/keyboard-controls.js';
import { MonthsDays } from './modules/months-days.js';
import './components/main.css';

export class Datepicker {
    public value: string;
    protected settings: ISettingsFull;
    protected root: HTMLElement;
    protected months: IMonthName[];
    protected weekDays: IMonthName[];
    protected active: boolean;
    protected currentMonth: number;
    protected currentYear: number;
    protected years: number[];
    protected mainBlock: MainBlock;
    protected keyBoardControl!: KeyboardControls;

    constructor(root: string | HTMLElement, settings?: ISettings) {
        const defaults: ISettingsFull = {
            month: DatepickerDate.thisMonth,
            year: DatepickerDate.thisYear,
            theme: 'light',
            openOn: 'click',
            lang: 'ru',
            autoClose: true,
            keyBoardControls: true,
        };
        this.settings = Object.assign(defaults, settings);

        if (typeof root === 'string') {
            const element = document.getElementById(root);
            if (element) {
                this.root = element;
            } else {
                throw Error(`Root element for Datepicker by id ${root} not found`);
            }
        } else {
            this.root = root;
        }

        this.value = '';
        this.active = false;

        const monthsDays = new MonthsDays(this.settings.lang);
        this.currentMonth = this.settings.month;
        this.currentYear = this.settings.year;
        this.months = monthsDays.months;
        this.years = [this.currentYear - 11, this.currentYear];
        this.weekDays = monthsDays.weekDays;

        this.mainBlock = new MainBlock(this.settings.theme, this.weekDays, this.months);
        this.root.innerHTML = this.mainBlock.template;

        if (!this.mainBlock.previousButton ||
            !this.mainBlock.nextButton ||
            !this.mainBlock.month ||
            !this.mainBlock.year ||
            !this.mainBlock.tbody ||
            !this.mainBlock.selectButton
        ) { return; }

        this.mainBlock.previousButton.addEventListener('click', this.previous);
        this.mainBlock.nextButton.addEventListener('click', this.next);
        this.mainBlock.month.addEventListener('click', this.onMonthSelected);
        this.mainBlock.year.addEventListener('click', this.onYearSelected);
        this.mainBlock.tbody.addEventListener('click', this.onBodyClicked);
        this.mainBlock.selectButton.addEventListener(this.settings.openOn, this.openOrClose);

        if (this.settings.keyBoardControls) {
            this.mainBlock.previousButton.addEventListener('keypress', this.previous);
            this.mainBlock.nextButton.addEventListener('keypress', this.next);
            this.mainBlock.month.addEventListener('keypress', this.onMonthSelected);
            this.mainBlock.year.addEventListener('keypress', this.onYearSelected);
            this.mainBlock.tbody.addEventListener('keypress', this.onBodyClicked);
            this.mainBlock.selectButton.addEventListener('keypress', this.openOrClose);
            this.keyBoardControl = new KeyboardControls(this.mainBlock);
        }
    }

    public open = () => {
        this.renderDays();
        this.mainBlock.showPicker();
        this.active = true;
    }

    public close = () => {
        this.mainBlock.hidePicker();
        this.active = false;
    }

    private renderDays() {
        this.mainBlock.currentElement = 'day';
        const daysInMonth = DatepickerDate.daysInMonth(this.currentYear, this.currentMonth);
        const firstDay = DatepickerDate.firstDay(this.currentYear, this.currentMonth);
        const previousMonthDay = DatepickerDate.previousMonthDays(this.currentYear, this.currentMonth - 1, firstDay - 1);

        this.mainBlock.resetBody();
        this.mainBlock.showHead();
        this.mainBlock.monthText = this.months[this.currentMonth].longName;
        this.mainBlock.yearText = this.currentYear.toString();

        let date = 1;
        for (let i = 0; i < 6; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < 7; j++) {

                let cell = null;
                if (i === 0 && j < firstDay) {
                    cell = new Cell({
                        text: previousMonthDay().toString(),
                        className: 'cell_light',
                        id: i * 7 + j,
                    });

                    cell.addAttr('data-month', 'previous');
                } else if (date > daysInMonth) {
                    cell = new Cell({
                        text: (date++ - daysInMonth).toString(),
                        className: 'cell_light',
                        id: i * 7 + j,
                    });

                    cell.addAttr('data-month', 'next');
                } else {
                    cell = new Cell({
                        text: date.toString(),
                        id: i * 7 + j,
                    });
                    if (date === DatepickerDate.todayDate &&
                        this.currentYear === DatepickerDate.thisYear &&
                        this.currentMonth === DatepickerDate.thisMonth
                    ) {
                        cell.addClass('cell_default');
                    }

                    date++;
                }

                row.appendChild(cell.template);
            }

            this.mainBlock.appendChild(row);
        }
    }

    private renderMonths() {
        this.mainBlock.currentElement = 'month';
        this.mainBlock.hideHead();
        this.mainBlock.resetBody();

        for (let i = 0; i < 3; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < 4; j++) {
                const index = i * 4 + j;
                const cell = new Cell({
                    text: this.months[index].shortName,
                    title: this.months[index].longName,
                    className: 'cell_big',
                    id: i * 4 + j,
                });

                if (index === DatepickerDate.thisMonth && this.currentYear === DatepickerDate.thisYear) {
                    cell.addClass('cell_default');
                }

                row.appendChild(cell.template);
            }

            this.mainBlock.appendChild(row);
        }
    }

    private renderYears() {
        this.mainBlock.currentElement = 'year';
        this.mainBlock.hideHead();
        this.mainBlock.resetBody();

        let year = this.years[0];
        for (let i = 0; i < 3; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < 4; j++) {
                const cell = new Cell({
                    text: year.toString(),
                    className: 'cell_big',
                    id: i * 4 + j,
                });

                if (year === DatepickerDate.thisYear) {
                    cell.addClass('cell_default');
                }

                row.appendChild(cell.template);
                year++;
            }

            this.mainBlock.appendChild(row);
        }
    }

    private previous = (e: Event | KeyboardEvent) => {
        if (e instanceof KeyboardEvent && e.code !== 'Enter') { return; }
        if (this.mainBlock.currentElement === 'day') {
            if (--this.currentMonth - 1 < 0) {
                this.currentMonth = 11;
                this.currentYear--;
            }

            this.renderDays();
        } else if (this.mainBlock.currentElement === 'year') {
            this.years[1] = this.years[0] - 1;
            this.years[0] = this.years[1] - 11;
            this.renderYears();
        } else {
            this.currentYear--;
            this.mainBlock.yearText = this.currentYear.toString();
        }
    }

    private next = (e: Event | KeyboardEvent) => {
        if (e instanceof KeyboardEvent && e.code !== 'Enter') { return; }
        if (this.mainBlock.currentElement === 'day') {
            if (++this.currentMonth + 1 > 11) {
                this.currentMonth = 0;
                this.currentYear++;
            }

            this.renderDays();
        } else if (this.mainBlock.currentElement === 'year') {
            this.years[0] = this.years[1] + 1;
            this.years[1] = this.years[0] + 11;
            this.renderYears();
        } else {
            this.currentYear++;
            this.mainBlock.yearText = this.currentYear.toString();
        }
    }

    private onDateSelected = (day: number) => {
        this.value = `${DatepickerDate.toTwoDigit(day)}/${DatepickerDate.toTwoDigit(this.currentMonth + 1)}/${this.currentYear}`;
        this.mainBlock.dateText = this.value;
        if (this.settings.autoClose) { this.close(); }
    }

    private onMonthSelected = (e: Event | KeyboardEvent) => {
        if (e instanceof KeyboardEvent && e.code !== 'Enter') { return; }
        this.mainBlock.currentElement === 'month' ? this.renderDays() : this.renderMonths();
        if (this.settings.keyBoardControls) {
            this.keyBoardControl.focusFirstElement(e);
        }
    }

    private onYearSelected = (e: Event | KeyboardEvent) => {
        if (e instanceof KeyboardEvent && e.code !== 'Enter') { return; }
        this.mainBlock.currentElement === 'year' ? this.renderDays() : this.renderYears();
        if (this.settings.keyBoardControls) {
            this.keyBoardControl.focusFirstElement(e);
        }
    }

    private onBodyClicked = (e: Event | KeyboardEvent) => {
        if (e instanceof KeyboardEvent && e.code !== 'Enter') { return; }
        const target = e.target;

        if (target instanceof HTMLTableCellElement) {
            const text = target.innerText;

            if (this.mainBlock.currentElement === 'day') {
                if (target.dataset.month) {
                    // @ts-ignore
                    this[target.dataset.month]();
                }
                this.onDateSelected(+text);
            } else if (this.mainBlock.currentElement === 'month') {
                const index = this.months.findIndex((m) => m.shortName === text);
                if (!~index) { return; }

                this.currentMonth = index;
                this.mainBlock.monthText = this.months[index].longName;
                this.renderDays();
            } else {
                this.currentYear = +text;
                this.mainBlock.yearText = text;
                this.renderMonths();
            }
        }

        if (this.settings.keyBoardControls) {
            this.keyBoardControl.focusFirstElement(e);
        }
    }

    private openOrClose = () => {
        !this.active ? this.open() : this.close();
    }
}
