type DatepickerElement = 'day' | 'month' | 'year';
type SupportedLanguage = 'ru';
type SupportedTheme = 'light';

interface IMonthName {
    longName: string;
    shortName: string;
}

interface ILanguage {
    weekDays: IMonthName[];
    months: IMonthName[];
}
