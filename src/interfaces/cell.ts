interface ICell {
    text: string;
    id: number;
    title?: string;
    className?: string;
}
