interface ISettings {
    month?: number;
    year?: number;
    theme?: SupportedTheme;
    openOn?: string;
    lang?: SupportedLanguage;
    autoClose?: boolean;
    keyBoardControls?: boolean;
}

interface ISettingsFull {
    month: number;
    year: number;
    theme: SupportedTheme;
    openOn: string;
    lang: SupportedLanguage;
    autoClose: boolean;
    keyBoardControls: boolean;
}
