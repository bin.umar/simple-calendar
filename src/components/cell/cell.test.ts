import { Cell } from './cell';

test('Creating cell and testing its methods', () => {
    const cell = new Cell({
        text: 'Jan',
        title: 'January',
        id: 10,
        className: 'cell'
    });

    expect(cell.template).toBeInstanceOf(HTMLTableCellElement);
    expect(cell['text']).toBe('Jan');
    expect(cell.template.title).toBe('January');
    expect(cell.template.dataset.id).toBe('10');
    expect(cell.template.classList[0]).toBe('cell');
});
