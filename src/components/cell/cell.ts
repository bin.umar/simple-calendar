import './cell.css';
import './cell_active.css';
import './cell_default.css';
import './cell_big.css';
import './cell_light.css';

export class Cell {
    protected text: string;
    protected cell: HTMLTableCellElement;

    constructor(obj: ICell) {
        this.text = obj.text;
        this.cell = document.createElement('td');
        this.cell.setAttribute('title', obj.title || obj.text);
        this.cell.setAttribute('tabindex', '0');
        this.cell.setAttribute('data-id', obj.id.toString());
        this.cell.classList.add('cell');

        if (obj.className) {
            this.addClass(obj.className);
        }

        const cellText = document.createTextNode(this.text);
        this.cell.appendChild(cellText);
    }

    public get template(): HTMLTableCellElement {
        return this.cell;
    }

    public addClass(className: string) {
        const classes = className.replace(/\s+/g, ' ').trim().split(' ');
        classes.forEach((c) => this.cell.classList.add(c));
    }

    public addAttr(name: string, value: string) {
        this.cell.setAttribute(name, value);
    }
}
