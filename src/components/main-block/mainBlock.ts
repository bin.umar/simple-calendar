import './datepicker.css';
import './datepicker_light.css';
import './datepicker__header.css';
import './__picker/datepicker__picker.css';
import './__picker/datepicker__picker__body.css';
import './__picker/datepicker__picker__control.css';
import './__picker/datepicker__picker__controls.css';
import './__picker/datepicker__picker__header.css';

export class MainBlock {
    public get thead(): HTMLTableSectionElement | null {
        return document.querySelector(`#${this.id} thead`);
    }

    public get tbody(): HTMLTableSectionElement | null {
        return document.querySelector(`#${this.id} tbody`);
    }

    public get date(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div[data-id=date]`);
    }

    public get picker(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div.datepicker__picker`);
    }

    public get month(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div[data-id=month]`);
    }

    public get year(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div[data-id=year]`);
    }

    public get selectButton(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div[data-id=select]`);
    }

    public get previousButton(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div[data-id=previous]`);
    }

    public get nextButton(): HTMLDivElement | null {
        return document.querySelector(`#${this.id} div[data-id=next]`);
    }

    public set dateText(data: string) {
        if (this.date) { this.date.innerHTML = data; }
    }

    public set monthText(data: string) {
        if (this.month) { this.month.innerText = data; }
    }

    public set yearText(data: string) {
        if (this.year) { this.year.innerText = data; }
    }

    public template: string;
    public currentElement: DatepickerElement;
    public id: string;
    protected theme: string;
    protected weekDays: IMonthName[];
    protected months: IMonthName[];

    constructor(theme: string, weekDays: IMonthName[], months: IMonthName[]) {
        this.template = '';
        this.theme = theme;
        this.currentElement = 'day';
        this.weekDays = weekDays;
        this.months = months;
        // generate unique id
        this.id = `datepicker_${Math.random().toString(36).substr(2, 9)}`;

        this.render();
    }

    public appendChild(element: HTMLTableRowElement) {
        if (this.tbody) this.tbody.appendChild(element);
    }

    public resetBody() {
        if (this.tbody) this.tbody.innerHTML = '';
    }

    public hideHead() {
        if (!this.thead) { return; }
        this.thead.style.display = 'none';
    }

    public showHead() {
        if (!this.thead) { return; }
        this.thead.style.display = 'table-header-group';
    }

    public showPicker() {
        if (!this.picker) { return; }
        this.picker.style.display = 'block';
    }

    public hidePicker() {
        if (!this.picker) { return; }
        this.picker.style.display = 'none';
    }

    private render() {
        let daysHeader = '';
        for (let i = 0; i < 7; i++) {
            daysHeader += `<th title="${this.weekDays[i].longName}" class="cell cell_light cell_header">
                            ${this.weekDays[i].shortName}
                           </th>
                        `;
        }

        this.template = `
            <div class="datepicker datepicker_${this.theme}" id="${this.id}">
                <div class="datepicker__header">
                    <div class="datepicker__select" tabindex="0" data-id="select">Выберите дату</div>
                    <div data-id="date"></div>
                </div>
                <div class="datepicker__picker shadow-xl">
                    <div class="datepicker__picker__header">
                        <div class="datepicker__picker__controls">
                            <div class="datepicker__picker__control" tabindex="0" data-id="month"></div>
                            <div class="datepicker__picker__control" tabindex="0" data-id="year"></div>
                        </div>
                        <div class="datepicker__picker__controls">
                            <div class="datepicker__picker__control" tabindex="0" data-id="previous">&#x25C2;</div>
                            <div class="datepicker__picker__control" tabindex="0" data-id="next">&#x25B8;</div>
                        </div>
                    </div>
                    <div class="datepicker__picker__body">
                        <table>
                            <thead>
                                <tr>
                                    ${daysHeader}
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        `;
    }
}
